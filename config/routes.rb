# frozen_string_literal: true

# Check out "config/routes" for the rest routes
Rails.application.routes.draw do
  scope module: "v1", path: "v1", constraints: { format: "json" } do
    scope module: "users" do
      post :login, to: "sessions#create"
      delete :logout, to: "sessions#destroy"
    end
  end
end
