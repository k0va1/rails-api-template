require "#{Rails.root}/lib/utils/jwt_manager"
require "#{Rails.root}/app/services/application_service"

require "dry/validation"
Dry::Validation.load_extensions(:monads)

Dry::Rails.container do
  config.features = %i[application_contract safe_params controller_helpers]

  # enable auto-registration in the lib dir
  auto_register!("lib")
  auto_register!("app/services")
  auto_register!("app/contracts")
  auto_register!("app/queries")
  auto_register!("app/serializers")

  register(
    "utils.jwt_manager",
    ::Utils::JwtManager.new,
  )
end

Dry::Rails::Railtie.reload
