require_relative "boot"

require "rails"
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"

Bundler.require(*Rails.groups)

module RailsApiTemplate
  class Application < Rails::Application
    config.load_defaults 6.0
    config.api_only = true

    # needed for omniauth
    config.session_store :cookie_store, key: "_interslice_session"
    config.middleware.use ActionDispatch::Cookies # Required for all session management
    config.middleware.use ActionDispatch::Session::CookieStore, config.session_options

    # needed for administrate
    config.middleware.use ActionDispatch::Flash
    config.middleware.use ::Rack::MethodOverride

    config.active_job.queue_adapter = :sidekiq

    # needed for logidze
    config.active_record.schema_format = :sql
    config.paths["config/routes.rb"].concat Dir[Rails.root.join("config/routes/*.rb")]

    # FIXME: rails HAS TO do it internaly, but doen't
    # this is the fast solution
    config.autoload_once_paths << "#{root}/app/serializers"
    config.autoload_once_paths << "#{root}/app/queries"

    config.active_storage.variant_processor = :vips

    config.generators do |g|
      g.test_framework :rspec, fixtures: false, view_specs: false, helper_specs: false,
        routing_specs: false, request_specs: false, controller_specs: false
    end
  end
end
