# frozen_string_literal: true

class ErrorsSerializer < ApplicationSerializer
  attributes :errors
end
