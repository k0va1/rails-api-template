# frozen_string_literal: true

module V1
  class UserSerializer < ::GenericSerializer
    attributes :first_name, :last_name
  end
end
