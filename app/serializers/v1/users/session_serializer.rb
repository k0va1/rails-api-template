# frozen_string_literal: true

module V1
  module Users
    class SessionSerializer < ::GenericSerializer
      include ::RailsApiTemplate::Deps["utils.jwt_manager"]

      attributes :token, :first_name, :last_name

      def token
        jwt_manager.encode({ user_id: represented.id })
      end
    end
  end
end
