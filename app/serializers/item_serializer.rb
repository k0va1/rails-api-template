# frozen_string_literal: true

class ItemSerializer < ApplicationSerializer
  attributes :data
end
