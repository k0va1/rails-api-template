# frozen_string_literal: true

class GenericSerializer < ApplicationSerializer
  attributes :id
end
