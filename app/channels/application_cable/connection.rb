# frozen_string_literal: true

module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = find_verified_user
    end

    private

    def find_verified_user
      token = request.params["token"]
      return reject_unauthorized_connection if token.blank?

      decoded_jwt = jwt_manager.decode(token)
      verified_user = User.find_by(id: decoded_jwt[:user_id])

      verified_user.presence || reject_unauthorized_connection
    rescue JWT::DecodeError
      reject_unauthorized_connection
    end

    def jwt_manager
      ::DapApi::Container["utils.jwt_manager"]
    end
  end
end
