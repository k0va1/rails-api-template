# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                 :bigint           not null, primary key
#  admin              :boolean          default(FALSE)
#  banned_at          :datetime
#  confirmation_token :string
#  email              :string
#  first_name         :string
#  last_ip            :string
#  last_name          :string
#  log_data           :jsonb
#  phone_number       :string
#  provider           :string
#  status             :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_users_on_email         (email) UNIQUE
#  index_users_on_phone_number  (phone_number) UNIQUE
#
class User < ApplicationRecord
end
