# frozen_string_literal: true

module V1
  module Users
    class SessionsController < ::V1::BaseController
      skip_before_action :authenticate_user, only: :create

      include RailsApiTemplate::Deps[
        login: "users.login_service",
        new: "sessions.new_contract",
      ]

      def create
        result = handle_create(params)

        case result
        in Success[:logged_in, user]
          render json: user, status: :created
        in Success[:code_sent, user]
          render json: { data: { signInStatus: "code_sent", message: "code was sent to your mobile number" } },
            status: :ok
        in Failure(Dry::Validation::Result)
          render json: { message: "Validation failed", errors: result.failure.errors.to_h },
            status: :unprocessable_entity
        in Failure
          status, _user = result.failure
          message = resolve_message(status, "services.users.login")

          render json: { message: message, errors: [] }, status: :bad_request
        end
      end

      private

      def handle_create(params)
        validation_result = yield new.call(params).to_monad
        login.call(validation_result.values.merge(last_ip: request.remote_ip))
      end

      def session_serializer
        # ::V1::Users::SessionSerializer
      end

      def resolve_message(status, path, _arguments = {})
        locale = I18n.default_locale
        full_path = "#{path}.#{status}"

        I18n.t(full_path, locale: locale, default: [:errors, :generic_response_error])
      end
    end
  end
end
