# frozen_string_literal: true

module V1
  class BaseController < ::ApplicationController
    before_action :authenticate_user

    include RailsApiTemplate::Deps["utils.jwt_manager"]

    def authenticate_user
      header = request.headers["Authorization"]
      if header.blank?
        render json: { message: "Authorization header should be passed" }, status: :unauthorized
        return
      end

      token = header.split.last

      if token.blank?
        render json: { message: "JWT token should be passed" }, status: :unauthorized
        nil
      else
        decoded_jwt = jwt_manager.decode(token)
        @current_user = User.find(decoded_jwt[:user_id])
        @current_user.update(last_ip: request.ip)
        @current_user
      end
    rescue ActiveRecord::RecordNotFound, JWT::DecodeError => e
      render json: { errors: e.message }, status: :unauthorized
    end

    attr_reader :current_user

    def params
      super.permit!.to_h
    end
  end
end
