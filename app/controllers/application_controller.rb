# frozen_string_literal: true

class ApplicationController < ActionController::API
  include Dry::Monads[:result, :do, :try]
  before_action :set_default_response_format
  wrap_parameters false

  protected

  def set_default_response_format
    request.format = :json
  end
end
