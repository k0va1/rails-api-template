# frozen_string_literal: true

module Paginateable
  DEFAULT_PER_PAGE = 20
  DEFAULT_PAGE = 1

  def paginate(scope, params)
    page = params[:page] || DEFAULT_PAGE
    per_page = params[:per_page] || DEFAULT_PER_PAGE

    records = scope.limit(per_page).offset(calc_offset(page, per_page))
    meta = build_meta(scope, page, per_page)

    [records, meta]
  end

  private

  def build_meta(scope, page, per_page)
    {
      page: page,
      per_page: per_page,
      total: scope.size,
      total_pages: (scope.size / per_page.to_f).ceil,
    }
  end

  def calc_offset(page, per_page)
    (page - 1) * per_page
  end
end
