# frozen_string_literal: true

class ApplicationQuery
  include Paginateable

  def call(params:); end
end
