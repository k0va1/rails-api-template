# frozen_string_literal: true

require "dry/monads"

class ApplicationService
  include Dry::Monads[:result, :do, :try]
end
