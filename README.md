# Example: backend api


## How to setup development env

* Install [Docker](https://www.docker.com/get-started) and docker-compose

* Prepare database `make db-prepare`

## Start backend server & background processing

`make dev`

OR

In first terminal: `docker-compose up rails`

In second terminal: `docker-compose up sidekiq`

Visit http://localhost:3000/

## Make commands

* `make install` install dependencies
* `make server` run Rails server and Anycable
* `make backend` run all backend services
* `make db-prepare` create databases, run migrations and seeds
* `make db-migrate` migrate existed database(test also)
* `make db-open` run psql
* `make stop` stop Rails & Sidekiq
* `make test` run tests
* `make get-docs` generate API documentation
* `make open-docs` open API documentation in browser
* `make cons` open rails console
* `make dive` open bash in container
* `make lint` run rubocop
* `make lint-fix` autofix rubocop offences

## Development

After pulling new updates run:

```
make install
make db-migrate
```

#### Run tests

All suite:

```console
make test
```

Specific file:

```console
make test spec/acceptance/activities_spec.rb
```
