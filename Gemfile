source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "2.7.2"

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem "rails", "~> 6.1.6.1"
# Use postgresql as the database for Active Record
gem "pg", ">= 0.18", "< 2.0"
# Use Puma as the app server
gem "puma", "~> 4.1"
gem "redis", "~> 4.0"
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

gem "image_processing", "~> 1.2"

# gem "anycable-rails", "~> 1.1"
gem "awesome_print"

gem "rack-cors"
gem "rack-attack"

gem "dry-rails", "~> 0.1"
gem "dry-monads"
gem "aasm"
gem "active_model_serializers", "~> 0.10.0"
gem "oj"
gem "jwt"

gem "dry-struct"
gem "bundler-audit"

gem "sidekiq"
gem "sidekiq-scheduler"

gem "logidze", "~> 1.1"

gem "rails-i18n"
gem "i18n_data"

# gem "aws-sdk-s3"
gem "counter_culture", "~> 2.0"
gem "after_commit_action"

group :development, :test do
  gem "bootsnap", require: false
  gem "rspec-rails", "~> 5.0.0"
  gem "rspec_api_documentation"
  gem "pry-rails"
  gem "pry-byebug"
  gem "factory_bot_rails"
  gem "bullet"
  gem "faker"
end

group :development do
  gem "listen", "~> 3.2"
  gem "annotate"
  gem "colorize"
  gem "letter_opener_web", "~> 2.0"
  gem "rubocop", "~> 1.23", require: false
  gem "rubocop-performance", require: false
  gem "rubocop-rails", require: false
  gem "rubocop-rspec", require: false
  gem "ruby-progressbar"
end

group :test do
  gem "shoulda-matchers", "~> 5.0"
  gem "database_cleaner-active_record"
  gem "json_matchers"
  gem "simplecov", require: false
  # gem 'database_cleaner-redis'
end
