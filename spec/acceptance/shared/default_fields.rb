# frozen_string_literal: true

module Shared
  module DefaultFields
    def default_meta_field
      response_field :meta,
        type: :object,
        properties: {
          page: { type: :integer, description: "Current page number" },
          perPage: { type: :integer, description: "Number of elements on page" },
          total: { type: :integer, description: "Total amount of elements" },
          totalPages: { type: :integer, description: "Total amount of pages" },
        },
        description: "Container for meta information like pagination"
    end
  end
end
