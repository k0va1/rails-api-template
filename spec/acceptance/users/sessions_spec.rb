require "rails_helper"
require "rspec_api_documentation/dsl"

resource "Sessions" do
  explanation "Sessions resource"

  post "/v1/login" do
    parameter :country_code, type: :string, required: true
    parameter :phone_number, type: :string, required: true
    parameter :otp_code, type: :string, required: false

    let!(:user) { create(:user, phone_number: "79778960951") }

    example "Login existing user" do
      params = {
        country_code: "7",
        phone_number: "9778960951",
      }

      do_request(params)

      expect(response_status).to eq(200)
    end

    example "With valid otp code" do
      code = create(:confirmation_code, :pending, user: user)

      params = {
        country_code: "7",
        phone_number: "9778960951",
        otp_code: code.code,
      }

      do_request(params)

      expect(response_body).to match_json_schema("v1/login/create")
      expect(response_status).to eq(201)
    end

    example "With invalid params" do
      params = {
        country_code: "7",
      }

      do_request(params)

      expect(response_status).to eq(422)
    end
  end
end
