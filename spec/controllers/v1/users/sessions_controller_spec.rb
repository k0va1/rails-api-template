# frozen_string_literal: true

require "rails_helper"

RSpec.describe ::V1::Users::SessionsController, type: :controller do
  describe "POST /v1/login" do
    subject { post :create, params: params }

    context "when user does not exist" do
      context "with valid params" do
        let(:params) do
          {
            country_code: "7",
            phone_number: "9778960951",
          }
        end

        it "sends sms confirmation code" do
          subject

          expect(response.status).to eq(200)
        end
      end
    end

    context "when user exists" do
      let!(:user) { create(:user, phone_number: "79778960951") }
      let!(:code) { create(:confirmation_code, code: "123456", user: user, status: "pending") }

      context "only with phone number" do
        let(:params) do
          {
            country_code: "7",
            phone_number: "9778960951",
          }
        end

        it "sends sms confirmation code" do
          subject

          expect(response.status).to eq(200)
        end
      end

      context "with phone number and otp code" do
        let(:params) do
          {
            country_code: "7",
            phone_number: "9778960951",
            otp_code: "123456",
          }
        end

        it "returns authentication token" do
          subject

          expect(response.status).to eq(201)
        end
      end

      context "with phone number and invalid otp code" do
        let(:params) do
          {
            country_code: "7",
            phone_number: "9778960951",
            otp_code: "123455",
          }
        end

        it "returns bad request" do
          subject

          expect(response.status).to eq(400)
        end
      end
    end
  end
end
