# frozen_string_literal: true

module AuthHelper
  def login(user)
    request.headers["Authorization"] = "Bearer #{::Utils::JwtManager.new.encode({ user_id: user.id })}"
  end
end
