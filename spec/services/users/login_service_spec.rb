# frozen_string_literal: true

require "rails_helper"

RSpec.describe Users::LoginService do
  subject(:login_service) { described_class.new.call(params) }

  context "when user exists" do
    let!(:user) { create(:user, phone_number: phone_number) }

    context "with given otp code" do
      let!(:confirmation_code) { create(:confirmation_code, :pending, code: "000000", user: user) }
      let(:params) { { phone_number: phone_number, otp_code: confirmation_code.code, last_ip: "192.168.0.1" } }

      context "when otp code matched" do
        it "returns success with user" do
          expect(login_service).to be_success
        end

        it "marks otp_code as confirmed" do
          login_service

          expect(confirmation_code.reload.status).to eq("confirmed")
        end

        it "updates user last_ip" do
          login_service

          expect(user.reload.last_ip).to eq("192.168.0.1")
        end
      end

      context "when invalid otp code" do
        let(:params) { { phone_number: phone_number, otp_code: "111111" } }

        it "returns failure with user" do
          expect(login_service).to be_failure
          expect(login_service.failure).to eq([:invalid_code, user])
        end
      end
    end

    context "without otp code" do
      let!(:confirmation_code) { create(:confirmation_code, :pending, code: "000000", user: user) }
      let(:params) { { phone_number: phone_number } }

      context "when user has pending code" do
        it "does not send new code" do
          expect { login_service }.not_to change(ConfirmationCode, :count)
        end
      end

      context "when all user codes have been expired" do
        before do
          confirmation_code.expire!
        end

        it "sends new code" do
          expect { login_service }.to change(ConfirmationCode, :count)
        end
      end
    end
  end

  context "when user does not exist" do
    it "creates new user" do
      expect { login_service }.to change(User, :count)
    end
  end
end
