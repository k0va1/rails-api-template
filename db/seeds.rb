# frozen_string_literal: true

require 'ruby-progressbar'

class Seeder
  class << self
    def create_progressbar(title, total)
      ProgressBar.create(title: title, total: total, format: "\e[1;92m%t %c/%C: |%B|\e[0m")
    end

    # with_progressbar("title", 10) do |pb|
    #   pb.increment
    # end
    def with_progressbar(title, total)
      pb = create_progressbar(title, total)
      yield(pb)
    end
  end
end
