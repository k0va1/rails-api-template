# frozen_string_literal: true

require "jwt"

module Utils
  class JwtManager
    def encode(data, exp = 1.year.from_now)
      data[:exp] = exp.to_i
      JWT.encode(data, secret)
    end

    def decode(token)
      decoded = ::JWT.decode(token, secret)[0]
      ::HashWithIndifferentAccess.new(decoded)
    end

    private

    def secret
      ENV["HMAC_SECRET"]
    end
  end
end
