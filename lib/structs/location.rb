# frozen_string_literal: true

require_relative "./types"

module Structs
  class Location < ::Dry::Struct
    attribute :lat, ::Types::Float
    attribute :lon, ::Types::Float
  end
end
