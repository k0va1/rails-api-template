require_relative "../../db/seeds"

namespace :seeds do
  desc "Create users"
  task users: :environment do
  end
end

# Order is important
desc "Run all seeds"
task seeds: %w[
  seeds:users
]
